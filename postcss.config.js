const basePath = process.env.KIRBY_BASE_PATH || "";

module.exports = {
    plugins: [
        require("postcss-import")(),
        require("postcss-url")({
            url: (asset) => `${basePath}${asset.url}`
        }),
        require("cssnano")
    ],
}
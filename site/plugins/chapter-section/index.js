panel.plugin("radikalklima/chapter-section-block", {
  blocks: {
    ["chapter-section"]: {
      computed: {
        textField() {
          return this.field("text");
        },
      },
      methods: {
        focus() {
          this.$refs.input.focus();
        },
      },
      template: `
    <div>
      <input
        type="text"
        placeholder="Überschrift (optional)"
        :value="content.headline"
        @input="update({ headline: $event.target.value })"
      />
      <k-writer
        ref="input"
        placeholder="Text"
        :marks="textField.marks"
        :value="content.text"
        @input="update({ text: $event })"
      />
    </div>
    `,
    },
  },
});

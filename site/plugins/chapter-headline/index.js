panel.plugin("radikalklima/chapter-headline-block", {
  blocks: {
    ["chapter-headline"]: `
      <input
        type="text"
        placeholder="Kapitel Überschrift"
        :value="content.headline"
        @input="update({ headline: $event.target.value })"
      />
    `,
  },
});

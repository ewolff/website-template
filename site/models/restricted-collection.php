<?php

class RestrictedCollectionPage extends Page
{
    public function template()
    {
        // try using the collection template first
        $intended = $this->kirby()->template('collection');
        if ($intended->exists() === true) {
            return $this->template = $intended;
        }
        return $this->template = $this->kirby()->template('default');
    }
}
<?php
return function($kirby, $pages, $page) {

  if($kirby->request()->is('POST') && get('submit')) {

    $data = [
      'email' => get('email'),
    ];
    $rules = [
      'email' => ['required', 'email'],
    ];
    $messages = [
      'email' => 'Please enter a valid email address',
    ];

    // some of the data is invalid
    if(invalid($data, $rules, $messages)) {

      return [
        'success' => false,
        'error'   => 'Bitte gib eine valide E-Mail-Adresse ein.'
      ];

    // the data is fine, let's send the email
    } else {
      try {

        $response = Requests::post(option('newsletter.api_url'), [], [
          'entity' => 'FormProcessor',
          'action' => 'newsletter',
          'api_key' => option('newsletter.api_key'),
          'key' => option('newsletter.key'),
          'newsletter_group_id' => '13',
          'json' => '1',
          'firstname' => '',
          'lastname' => '',
          'email' => get('email')
         ]);

        $jsonResponse = json_decode($response->body);

        if (isset($jsonResponse->is_error)) {
          throw new Exception($jsonResponse->error_message, 1);
        }

      } catch (Exception $error) {

        if(option('debug')) {
          $error = $error->getMessage();
          $response = $response;
        }

        return [
          'success'  => false,
          'error'    => $error,
          'response' => $response
        ];
      }

      return [
        'success'  => true
      ];
    }
  }

};
<a class="<?= classNames("event", ["event--bold" => $isBold]) ?>" href="<?= $event->url() ?>">
  <div class="event__date-time">
    <time class="event__date"><?= $event->date()->toDate("d.m") ?></time>
    <time class="event__time"><?= $event->date()->toDate("H:i") ?> Uhr</time>
  </div>
  <div class="event__text">
    <h3 class="event__title"><?= $event->title() ?></h3>
    <p class="event__description"><?= $event->description() ?></p>
  </div>
  <svg class="event__arrow" viewBox="0 0 15 24" fill="none">
    <path d="M2 2L12 12L2 22" stroke="black" stroke-width="4"/>
  </svg>
</a>
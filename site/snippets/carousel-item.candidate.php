<a class="<?= classnames("carousel-item", "carousel-item--candidate", [
  "carousel-item--bold" => $isBold
]) ?>" href="<?= $url ?>">
  <?php if ($image = $previewImage->toFile()): ?>
  <img src="<?= $image->url() ?>" alt="" loading="lazy">
  <?php endif; ?>
  <div class="carousel-item-preview">
    <?php if (Str::length($title) < 70): ?>
    <h3 class="carousel-item-title"><?= $title ?></h3>
    <?php else: ?>
    <h3 class="carousel-item-title h4"><?= $title ?></h3>
    <?php endif; ?>
    <?php if (isset($description)): ?>
    <p><?= $description ?></p>
    <?php endif; ?>
  </div>
</a>
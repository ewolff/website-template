<div class="sibling-navigator">
  <?php if ($prev = $page->prev()): ?>
  <a class="sibling-navigator__prev" href="<?= $prev->url() ?>">
    <div class="sibling-navigator__label"><?= $page->parent()->previousPage() ?></div>
    <div class="h3"><?= $prev->title() ?></div>
  </a>
  <?php endif; ?>
  <?php if ($next = $page->next()): ?>
  <a class="sibling-navigator__next" href="<?= $next->url() ?>">
    <div class="sibling-navigator__label"><?= $page->parent()->nextPage() ?></div>
    <div class="h3"><?= $next->title() ?></div>
  </a>
  <?php endif; ?>
</div>
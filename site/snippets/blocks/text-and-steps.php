<div class="text-and-steps grid">
  <div class="text-and-steps__text">
    <?php if ($block->headline()->isNotEmpty()): ?>
    <h2><?= $block->headline() ?></h2>
    <?php endif; ?>
    <?= $block->text()->kt() ?>
    <div class="text-and-steps__buttons">
    <?php if ($block->buttonText()->isNotEmpty() && $block->buttonLink()->isNotEmpty()): ?>
    <a href="<?= $block->buttonLink() ?>" class="button">
      <?= $block->buttonText() ?>
    </a>
    <?php endif; ?>
    <?php if ($block->linkText()->isNotEmpty() && $block->linkUrl()->isNotEmpty()): ?>
    <a href="<?= $block->linkUrl() ?>" class="link">
      <?= $block->linkText() ?>
    </a>
    <?php endif; ?>
    </div>
  </div>
  <?php if ($block->steps()->toStructure()->count() > 0): ?>
  <aside class="text-and-steps__steps">
    <ol>
    <?php foreach ($block->steps()->toStructure() as $step): ?>
      <li><div>
        <h3><?= $step->step() ?></h3>
        <?= $step->text() ?>
      </div></li>
    <?php endforeach; ?>
    </ol>
  </aside>
  <?php endif; ?>
</div>

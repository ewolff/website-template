<div class="text-and-list grid">
  <?php if ($block->headline()->isNotEmpty()): ?>
  <h2 class="text-and-list__headline"><?= $block->headline() ?></h2>
  <?php endif; ?>
  <div class="text-and-list__text">
    <?= $block->text()->kt() ?>
  </div>
  <?php if ($block->list()->toStructure()->count() > 0): ?>
  <aside class="text-and-list__list">
  <?php if ($block->listHeadline()->isNotEmpty()): ?>
  <h3><?= $block->listHeadline() ?></h3>
  <?php endif; ?>
  <?php foreach ($block->list()->toStructure() as $item): ?>
  <p><?= $item->item() ?></p>
  <?php endforeach; ?>
  </aside>
  <?php endif; ?>
</div>

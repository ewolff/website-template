<div class="intro-with-title grid">
  <h1><?= $page->title() ?></h1>
  <div class="intro-with-title__text blocks">
    <?= $block->text() ?>
  </div>
</div>
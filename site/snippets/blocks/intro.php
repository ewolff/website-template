<div class="intro">
  <img class="intro__background-image" src="<?=$block->image()->toFile()->url() ?>" alt="">
  <div class="diagonal diagonal--overlap">
    <div class="intro__content grid">
      <h1 class="intro__headline"><?= $block->headline() ?></h1>
      <p class="intro__text"><?= $block->text() ?></p>
      <?php if ($block->showSocialNetworks()->isTrue()): ?>
      <ul class="intro__social-networks">
        <?php foreach($site->networks()->toStructure() as $network): ?>
        <li>
          <a href="<?=$network->link() ?>">
            <img
            class="intro__social-network"
              src="<?=$network->icon()->toFile()->url() ?>"
              alt="<?=$network->name() ?>">
          </a>
        </li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>
    </div>
  </div>
</div>
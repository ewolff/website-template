<?php
  $diff = \Carbon\Carbon::parse("2021-09-26 08:00")
    ->locale(I18n::locale())
    ->fromNow([
      "options" =>
        \Carbon\Carbon::ROUND
        | \Carbon\Carbon::ONE_DAY_WORDS
        | \Carbon\Carbon::TWO_DAY_WORDS
      ]);

  $diff = ucfirst($diff);
?>

<div class="<?= classNames("countdown", [
  "countdown--bold" => $block->style() == "bold",
  "diagonal" => $block->style() == "bold"
])?>">
  <div class="countdown__text">
    <h2><u><?= $diff ?></u> kannst du deine Stimme für ein klimapositives und sozial gerechtes Berlin abgeben.</h2>
    <p>Am <strong>26.09.2021</strong> von 08:00 bis 18:00 Uhr sind die Wahllokale in Berlin geöffnet.</p>
  </div>
</div>
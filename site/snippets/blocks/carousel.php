<?php
  if ($block->automaticPages()->isTrue()) {
    $items = $block->chosenParentPages()->toPages()->children();
    $items = $items->sortBy('date', 'desc')->limit($block->maximumItems()->or(10)->toInt());
  } else {
    $items =  $block->chosenSpecificPages()->toPages();
  }
?>

<div class="<?= classNames("carousel", "grid", [
  "carousel--bold" => $block->style() == "bold",
  "diagonal" => $block->style() == "bold"
])?>">
  <h2 class="carousel__title"><?= $block->headline() ?></h2>
  <?php if ($moreLink = $block->moreLink()->toPage() ): ?>
  <a class="carousel__more-link link" href="<?= $moreLink->url() ?>">
    <?= $block->linkText()->or($moreLink->title()) ?>
  </a>
  <?php endif; ?>
  <div class="carousel__horizontal">
  <?php foreach ($items as $carouselItem): ?>

    <?php if ($carouselItem->intendedTemplate() == "default"): ?>
    <?php
      snippet("carousel-item.default", [
        "url" => $carouselItem->url(),
        "title" => $carouselItem->title(),
        "description" => $carouselItem->description(),
        "siteLinkText" => $block->siteLinkText(),
        "isBold" => $block->style() == "bold"
      ]);
    ?>
    <?php endif; ?>

    <?php if ($carouselItem->intendedTemplate() == "question"): ?>
    <?php
      snippet("carousel-item.question", [
        "url" => $carouselItem->url(),
        "title" => $carouselItem->title(),
        "description" => $carouselItem->description(),
        "siteLinkText" => $block->siteLinkText(),
        "isBold" => $block->style() == "bold"
      ]);
    ?>
    <?php endif; ?>

    <?php if ($carouselItem->intendedTemplate() == "blog-post"): ?>
    <?php
      snippet("carousel-item.blog-post", [
        "url" => $carouselItem->url(),
        "title" => $carouselItem->title(),
        "description" => $carouselItem->description(),
        "date" => $carouselItem->date(),
        "previewImage" => $carouselItem->previewImage(),
        "isBold" => $block->style() == "bold"
      ]);
    ?>
    <?php endif; ?>

    <?php if ($carouselItem->intendedTemplate() == "press-release"): ?>
    <?php
      snippet("carousel-item.press-release", [
        "url" => $carouselItem->url(),
        "title" => $carouselItem->title(),
        "description" => $carouselItem->description(),
        "date" => $carouselItem->date(),
        "isBold" => $block->style() == "bold"
      ]);
    ?>
    <?php endif; ?>

    <?php if ($carouselItem->intendedTemplate() == "press-external"): ?>
    <?php
      snippet("carousel-item.press-external", [
        "externalLink" => $carouselItem->externalLink(),
        "title" => $carouselItem->title(),
        "description" => $carouselItem->description(),
        "date" => $carouselItem->date(),
        "isBold" => $block->style() == "bold"
      ]);
    ?>
    <?php endif; ?>

    <?php if ($carouselItem->intendedTemplate() == "livestream"): ?>
    <?php
      snippet("carousel-item.livestream", [
        "url" => $carouselItem->url(),
        "title" => $carouselItem->title(),
        "description" => $carouselItem->description(),
        "date" => $carouselItem->date(),
        "previewImage" => $carouselItem->previewImage(),
        "isBold" => $block->style() == "bold"
      ]);
    ?>
    <?php endif; ?>

    <?php if ($carouselItem->intendedTemplate() == "candidate"): ?>
    <?php
      $landesliste = page('landesliste')->candidates()->toPages();
      $bvvliste = $site->children()
        ->filterBy('intendedTemplate', 'district')
        ->filterBy('id', '!=', 'landesliste')
        ->filter(function ($district) use ($carouselItem) {
          return $district->candidates()->toPages()->findById($carouselItem->id());
        })
        ->first();

      if ($landesliste) {
        $landeslistenPlatz = $carouselItem->indexOf($landesliste) + 1;
        $landeslisteDescription = "Landesliste Platz $landeslistenPlatz";
      }
      if ($bvvliste) {
        $bvvlistenPlatz = $carouselItem->indexOf($bvvliste->candidates()->toPages()) + 1;
        $bvvlistenDescription = $bvvliste->title() . " Platz " . $bvvlistenPlatz;
      }
      $description = join("<br>", [($landeslisteDescription ?? ""), ($bvvlistenDescription ?? "")]);
    ?>
    <?php
      snippet("carousel-item.candidate", [
        "url" => $carouselItem->url(),
        "title" => $carouselItem->title(),
        "description" => $description ?? NULL,
        "previewImage" => $carouselItem->previewImage(),
        "isBold" => $block->style() == "bold"
      ]);
    ?>
    <?php unset($landeslistenPlatz, $landeslisteDescription, $bvvlistenPlatz, $bvvlistenDescription); ?>
    <?php endif; ?>

  <?php endforeach; ?>
  </div>
</div>
<div class="logo-wall grid">
  <?php if ($block->headline()->isNotEmpty()): ?>
  <h2 class="logo-wall__headline"><?= $block->headline() ?></h2>
  <?php endif; ?>
  <div class="logo-wall__images">
  <?php foreach ($block->images()->toStructure() as $image): ?>
    <?php if ($image->link()->isNotEmpty()): ?>
      <a href="<?= $image->link() ?>">
        <img class="logo-wall__image" src="<?= $image->image()->toFile()->url() ?>" alt="">
      </a>
    <?php else: ?>
      <img class="logo-wall__image" src="<?= $image->image()->toFile()->url() ?>" alt="">
    <?php endif; ?>
  <?php endforeach; ?>
  </div>
</div>
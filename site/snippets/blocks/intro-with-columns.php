<div class="intro-with-columns grid">
  <div class="intro-with-columns__background"></div>
  <div class="intro-with-columns__text-top">
    <h1 class="intro-with-columns__headline"><?= $block->headline() ?></h1>
    <?= $block->textTop()->kt() ?>
  </div>
  <div class="intro-with-columns__text-bottom">
    <?= $block->textBottom()->kt() ?>
  </div>
  <div class="intro__with-columns-right">
    <div class="intro-with-columns__images">
      <?php foreach ($block->rightImages()->toFiles() as $image): ?>
        <img src="<?= $image->url() ?>">
      <?php endforeach; ?>
    </div>
    <div class="intro-with-columns__right-text">
      <?= $block->rightText()->kt() ?>
    </div>
  </div>
</div>
<?php
  $futureEvents = $site->find('termine')->children()->filter(function ($event) {
    return $event->date()->toDate() > time() - 21600; # in the future or 6 hours ago;
  });
  if ($block->filterCategory()->isNotEmpty()) {
    $futureEvents = $futureEvents->filterBy('category', $block->filterCategory());
  }
  $futureEvents = $futureEvents->sortBy('date', 'asc')->limit($block->maximumItems()->or(3)->toInt());
  if ($futureEvents->count() == 0) {
    return;
  }
?>
<div class="<?= classNames("events-preview", "grid", [
  "events-preview--bold" => $block->style() == "bold",
  "diagonal" => $block->style() == "bold"
])?>">
  <h2 class="events-preview__title"><?= $block->headline() ?></h2>
  <?php foreach ($futureEvents as $event): ?>
  <?php snippet("event", [
    "event" => $event,
    "isBold" => $block->style() == "bold"
    ]);
  ?>
  <?php endforeach; ?>
  <?php if ($block->linkText()->isNotEmpty()): ?>
  <a class="events-preview__more-link link" href="<?= $site->find('termine')->url() ?>">
    <?= $block->linkText() ?>
  </a>
  <?php endif; ?>
</div>
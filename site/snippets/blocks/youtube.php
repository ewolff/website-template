<div class="youtube">
  <?php if ($block->headline()->isNotEmpty()): ?>
  <h2 class="youtube__headline"><?= $block->headline() ?></h2>
  <?php endif; ?>

  <div class="youtube__container">
    <a id="youtube-<?=$block->youtubeID() ?>" href="https://www.youtube.com/watch?v=<?=$block->youtubeID() ?>" class="youtube__video">
      <img class="youtube__thumbnail" src="<?= $block->thumbnail()->toFile()->url() ?>" alt="">
      <img class="youtube__play-button" src="/assets/images/play.svg" alt="">
    </a>
  </div>
  <script type="module">
    const mediaElement = document.querySelector('#youtube-<?=$block->youtubeID() ?>');
    mediaElement.addEventListener('click', (event) => {
      event.preventDefault();
      mediaElement.innerHTML = '<iframe frameborder="0" src="https://youtube.com/embed/<?=$block->youtubeID() ?>?autoplay=1" allowfullscreen="allowfullscreen"></iframe>';
      mediaElement.classList.add('youtube__video--playing')
    }, false);
  </script>

  <?= $block->youtubeUrl() ?>

  <?php if ($block->text()->isNotEmpty()): ?>
  <div class="youtube__text">
    <?= $block->text() ?>
  </div>
  <?php endif; ?>
</div>

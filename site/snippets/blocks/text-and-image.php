<div class="<?= classNames("text-and-image", "grid", [
  "text-and-image--bold" => $block->style() == "bold",
  "diagonal" => $block->style() == "bold"
])?>">
  <h2 class="text-and-image__headline"><?= $block->headline() ?></h2>
  <div class="text-and-image__text">
    <?= $block->text()->kt() ?>
    <?php $links = $block->links()->toStructure(); ?>
    <?php if ($links->count()): ?>
    <ul class="text-and-image__links">
      <?php foreach($links as $link): ?>
      <?php
        $url = $link->external()->isTrue()
          ? $link->url()
          : $link->page()->toPage()->url();
      ?>
      <li>
        <a href="<?= $url ?>" class="link"><?= $link->text() ?></a>
      </li>
      <?php endforeach; ?>
    </ul>
    <?php endif; ?>
  </div>
  <div class="text-and-image__images" style="--columns: <?= $block->imageColumns() ?>">
  <?php foreach ($block->chosenImages()->toFiles() as $image): ?>
    <img class="text-and-image__image" src="<?= $image->url() ?>" alt="">
  <?php endforeach; ?>
  </div>
</div>
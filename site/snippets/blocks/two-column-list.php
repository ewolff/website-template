<div class="two-column-list">
  <h3><?= $block->headline() ?></h3>
  <dl>
  <?php foreach ($block->list()->toStructure() as $row): ?>
    <dt><?= $row->left() ?></dt>
    <dd><?= $row->right() ?></dd>
  <?php endforeach; ?>
  </dl>
</div>
<a class="collection-items__item" href="<?= $item->url() ?>">
  <?php if ($image = $item->previewImage()->toFile()): ?>
  <img src="<?= $image->url() ?>" alt="" loading="lazy">
  <?php endif; ?>
  <div class="collection-items__preview">
    <span class="collection-items__label">Livestream</span>
    <h2 class="collection-items__title"><?= $item->title() ?></h2>
    <?php if (!$image && $description = $item->description()): ?>
    <p><?= $description->kti() ?></p>
    <?php endif; ?>
    <time class="collection-items__date" datetime="<?= $item->date() ?>">
      <?= \Carbon\Carbon::parse($item->date()->toDate())->locale(I18n::locale())->isoFormat("DD. MMMM YYYY") ?>
    </time>
  </div>
</a>
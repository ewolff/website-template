<nav id="main-navigation" class="<?= classNames("nav", [
  "nav--blackAndActiveIsWhite" => $page->navigationStyle() == "blackAndActiveIsWhite",
  "nav--whiteAndActiveIsWhite" => $page->navigationStyle() == "whiteAndActiveIsWhite",
  "nav--whiteAndActiveIsBlack" => $page->navigationStyle() == "whiteAndActiveIsBlack",
  "nav--showNavigationGradient" => $page->showNavigationGradient()->isTrue()
]) ?>" aria-labelledby="navigation-toggle" aria-expanded="false">
  <div class="nav__container grid">
    <a href="<?=url("/") ?>" class="nav__logo">
      <img src="<?= $site->logo()->toFile()->url() ?>" alt="<?= $site->title() ?>">
    </a>
    <button id="navigation-toggle" class="nav__menu-button" aria-label="Toggle Navigation Menu">
      Menü
    </button>
    <div class="nav__links">
    <?php $index = 1; foreach($site->pages()->listed() as $topPage): ?>
      <a
        href="<?=$topPage->url() ?>"
        class="<?= classNames("nav__item", ["nav__item--active" => $topPage->isActive() or $topPage->categories()->toPages()->findOpen()]) ?>">
        <span class="nav__link"><?=$topPage->title()?></span>
      </a>
      <?php $index++; ?>
    <?php endforeach; ?>
    </div>
  </div>
</nav>

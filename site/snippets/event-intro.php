<header class="event-intro">
  <h1 class="event-intro__headline"><?= $event->title() ?></h1>
  <p class="event-intro__description"><?= $event->description() ?></p>
  <div class="event-intro__meta">
    <?php if ($icon = $site->image("icon-calendar.svg")): ?>
    <img src="<?= $icon->url() ?>" alt="">
    <?php endif; ?>
    <div class="event-intro__time">
      <time class="event-intro__date" datetime="<?= $event->date() ?>">
        <?= $event->date()->toDate("d. F Y") ?>
      </time>
      <time class="event-intro__from-to">
        <?= $event->date()->toDate("H:i") ?>
        <?php if ($event->toTime()->isNotEmpty()): ?>
        <span> - <?= $event->toTime()->toDate("H:i") ?></span>
        <?php endif; ?>
      </time>
    </div>
    <?php if ($icon2 = $site->image("icon-place.svg")): ?>
    <img src="<?= $icon2->url() ?>" alt="">
    <?php endif; ?>
    <div class="event-intro__place">
    <?= $event->place() ?>
    </div>
    <?php if ($event->registerLink()->isNotEmpty()): ?>
    <a class="event-intro__button button" href="<?= $event->registerLink() ?>">
      <?= $event->registerLinkText() ?>
    </a>
    <?php endif; ?>
  </div>
  <?php if ($event->registerLink()->isNotEmpty()): ?>
  <?php endif; ?>
  <?php if ($image = $event->previewImage()->toFile()): ?>
  <img class="event-intro__image" src="<?= $image->url() ?>" alt="">
  <?php endif; ?>
</header>

<?php if ($event->text()->isNotEmpty()): ?>
<?= $event->text() ?>
<?php endif; ?>

<div class="event-intro__bottom">
  <?php if ($event->registerLink()->isNotEmpty()): ?>
  <a class="button" href="<?= $event->registerLink() ?>">
    <?= $event->registerLinkText() ?>
  </a>
  <?php endif; ?>
</div>

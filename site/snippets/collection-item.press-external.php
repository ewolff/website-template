<a class="collection-items__item" href="<?= $item->externalLink() ?>">
  <div class="collection-items__preview">
    <span class="collection-items__label">Aus der Presse</span>
    <h2 class="collection-items__title"><?= $item->title() ?></h2>
    <?php if ($description = $item->description()): ?>
    <p><?= $description->kti() ?></p>
    <?php endif; ?>
    <time class="collection-items__date" datetime="<?= $item->date() ?>">
      <?= \Carbon\Carbon::parse($item->date()->toDate())->locale(I18n::locale())->isoFormat("DD. MMMM YYYY") ?>
    </time>
  </div>
</a>
<div class="<?= classNames("district-list", [
  "district-list--bold" => isset($style) && $style == "bold",
  "diagonal" => isset($style) && $style == "bold"
])?>">
  <div class="district-list__content">
    <h2 class="district-list__title"><?= $list->title() ?></h2>
    <ol class="district-list__list">
    <?php foreach ($candidates = $list->candidates()->toPages()->limit($limit ?? 50) as $candidate): ?>
      <li>
        <a
          class="<?= classNames([
            "district-list__candidate-neutral" => !isset($style) || $style == "neutral",
            "district-list__candidate-bold" => isset($style) && $style == "bold"
          ]) ?>"
          href="<?= $candidate->url() ?>">
          <div class="district-list__position">
            <?= $candidates->indexOf($candidate) + 1 ?>
          </div>
          <?php if($photo = $candidate->previewImage()->toFile()): ?>
          <img class="district-list__photo" src="<?= $photo->url() ?>" alt="">
          <?php else: ?>
          <div class="district-list__photo"></div>
          <?php endif; ?>
          <div class="district-list__name"><?= $candidate->title() ?></div>
          <svg class="district-list__arrow" viewBox="0 0 15 24" fill="none">
            <path d="M2 2L12 12L2 22" stroke="black" stroke-width="4"/>
          </svg>
        </a>
      </li>
    <?php endforeach ?>
    </ol>
  </div>
</div>
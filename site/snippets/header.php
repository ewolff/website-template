<!doctype html>
<html lang="<?= $kirby->language() ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $page->isHomePage() ? $site->title() : $page->title() . " | " . $site->title() ?></title>

    <?php snippet("meta-tags", [
        "siteName" => $site->title(),
        "title" => $page->isHomePage() ? $site->title() : $page->title() . " | " . $site->title(),
        "author" => $site->title(),
        "twitter" => "@radikalklima",
        "description" => $page->description()->or($site->description()),
        "image" => $page->previewImage()->or($site->previewImage())->toFile()->url()
    ]); ?>

    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-Black.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-BlackItalic.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-Bold.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-BoldItalic.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-Hairline.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-HairlineItalic.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-Heavy.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-HeavyItalic.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-Italic.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-Light.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-LightItalic.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-Medium.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-MediumItalic.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-Regular.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-Semibold.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-SemiboldItalic.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-Thin.woff2" />
    <link rel="preload" href="<?= $site->url() ?>/assets/fonts/lato/Lato-ThinItalic.woff2" />

    <?php if (isset($disable_auto_tel_detection) && $disable_auto_tel_detection): ?>
        <meta name="format-detection" content="telephone=no">
    <?php endif; ?>

    <?php snippet("favicon") ?>

    <?= option("css.compiled")
        ? css("assets/css/main.compiled.css")
        : css("assets/css/main.css") ?>

    <?= js("assets/js/what-input.min.js", ["defer" => true]) ?>

</head>
<body>

<?php snippet("navigation") ?>

<a class="<?= classnames("carousel-item", "carousel-item--question", [
  "carousel-item--bold" => $isBold
]) ?>" href="<?= $url ?>">
  <?php if (Str::length($title) < 70): ?>
  <h3 class="carousel-item-title"><?= $title ?></h3>
  <?php else: ?>
  <h3 class="carousel-item-title h4"><?= $title ?></h3>
  <?php endif; ?>
  <p class="carousel-item-text"><?= $description->kti() ?></p>
  <?php if ($siteLinkText->isNotEmpty()): ?>
  <span class="carousel-item-link link"><?= $siteLinkText ?></span>
  <?php endif; ?>
</a>
<footer class="footer grid">

  <form action="/newsletter" method="post" class="footer__newsletter-form">
    <fieldset>
      <legend class="h3 no-hyphens"><?= $site->newsletterHeadline() ?></legend>
      <label for="email" class="p"><?= $site->newsletterDescription()->kti() ?></label>
      <input class="footer__newsletter-input" id="email" type="email" name="email" placeholder="<?= $site->newsletterInput() ?>">
      <input class="footer__newsletter-button button" type="submit" name="submit" value="<?= $site->newsletterButtonText() ?>">
    </fieldset>
  </form>
  <ul class="footer__primary-navigation">
  <?php foreach($site->pages()->listed() as $primaryPage): ?>
    <li><a href="<?=$primaryPage->url() ?>" class="footer__link link"><?=$primaryPage->title()?></a></li>
  <?php endforeach; ?>
  </ul>
  <ul class="footer__secondary-navigation">
  <?php foreach($site->secondaryNavigation()->toPages() as $secondaryPage): ?>
    <li><a href="<?=$secondaryPage->url() ?>" class="footer__link link"><?=$secondaryPage->title()?></a></li>
  <?php endforeach; ?>
    <li><a href="/panel/pages/<?= str_replace("/", "+", $page->uri()); ?>" class="footer__link link">Mitglieder-Login</a></li>
  </ul>
  <ul class="footer__ternary-navigation">
  <?php foreach($site->ternaryNavigation()->toPages() as $ternaryPage): ?>
    <li><a href="<?=$ternaryPage->url() ?>" class="footer__link link"><?=$ternaryPage->title()?></a></li>
  <?php endforeach; ?>
  </ul>
  <ul class="footer__social-networks">
  <?php foreach($site->networks()->toStructure() as $network): ?>
    <li>
      <a href="<?=$network->link() ?>">
        <img
          class="footer__social-network"
          src="<?=$network->icon()->toFile()->url() ?>"
          alt="<?=$network->name() ?>">
      </a>
    </li>
    <?php endforeach; ?>
  </ul>

</footer>

<script type="module">
  const d = document;
  const button = d.querySelector("#navigation-toggle");
  const nav = d.querySelector("#main-navigation");
  button.addEventListener("click", () => {
    nav.setAttribute('aria-expanded', nav.getAttribute('aria-expanded') !== "true")
  });
</script>

</body>
</html>

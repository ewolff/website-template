<a class="<?= classnames("carousel-item", "carousel-item--press-release", [
  "carousel-item--bold" => $isBold
]) ?>" href="<?= $url ?>">
  <div class="carousel-item-preview">
    <span class="carousel-item-label">Pressemitteilung</span>
    <?php if (Str::length($title) < 70): ?>
    <h3 class="carousel-item-title"><?= $title ?></h3>
    <?php else: ?>
    <h3 class="carousel-item-title h4"><?= $title ?></h3>
    <?php endif; ?>
    <?php if ($description->isNotEmpty()): ?>
    <p><?= $description->kti() ?></p>
    <?php endif; ?>
    <time class="carousel-item-date" datetime="<?= $date ?>">
      <?= \Carbon\Carbon::parse($date->toDate())->locale(I18n::locale())->isoFormat("DD. MMMM YYYY") ?>
    </time>
  </div>
</a>
<!--  <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/apple-touch-icon.png">-->
  <link rel="icon" type="image/png" sizes="32x32" href="<?= $site->url() ?>/assets/images/favicon-32x32.png">
<!--  <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon-16x16.png">-->
<!--  <link rel="manifest" href="/assets/site.webmanifest">-->
<!--  <link rel="mask-icon" href="/assets/images/safari-favicon.svg" color="#000000">-->
<!--  <link rel="shortcut icon" href="/assets/images/favicon.ico">-->
<!--  <meta name="msapplication-TileColor" content="#00eccd">-->
<!--  <meta name="msapplication-config" content="/assets/images/browserconfig.xml">-->
  <meta name="theme-color" content="#ffffff">
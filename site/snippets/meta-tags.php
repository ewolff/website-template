<meta name="description" content="<?= $description ?>" >
<meta name="author" content="<?= $author ?>">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="<?= $twitter ?>">
<meta name="twitter:title" content="<?= $title ?>">
<meta name="twitter:description" content="<?= $description ?>">
<meta name="twitter:image" content="<?= $image ?>">
<meta property="og:site_name" content="<?= $siteName ?>">
<meta property="og:url" content="<?= $page->url() ?>">
<meta property="og:type" content="object">
<meta property="og:title" content="<?= $title ?>">
<meta property="og:description" content="<?= $description ?>">
<meta property="og:image" content="<?= $image ?>">
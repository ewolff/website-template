<div class="collection-items">
<?php foreach ($items as $item): ?>

<?php if ($item->intendedTemplate() == "blog-post"): ?>
<?php snippet("collection-item.blog-post", compact("item")) ?>
<?php endif; ?>

<?php if ($item->intendedTemplate() == "livestream"): ?>
<?php snippet("collection-item.livestream", compact("item")) ?>
<?php endif; ?>

<?php if ($item->intendedTemplate() == "press-release"): ?>
<?php snippet("collection-item.press-release", compact("item")) ?>
<?php endif; ?>

<?php if ($item->intendedTemplate() == "press-external"): ?>
<?php snippet("collection-item.press-external", compact("item")) ?>
<?php endif; ?>

<?php endforeach ?>
</div>
<?php

return [
    'css.compiled' => false,
    'debug' => true,
    'languages' => true,
    'schnti.cachebuster.active' => false,
    'newsletter.api_url' => 'http://httpbin.org/post',
    'newsletter.api_key' => 'api_key',
    'newsletter.key' => 'key',
    'smartypants' => true,
    'auth' => [
        'methods' => ['password', 'password-reset']
    ]
];

<?php snippet("header") ?>

<main class="grid blocks district">

<?php snippet("district-list", ["list" => $page]) ?>

<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

</main>

<?php snippet("footer") ?>

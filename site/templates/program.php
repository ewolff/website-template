<?php snippet("header") ?>

<?php
function encodeParam($parameter) {
  return urlencode(Str::replace($parameter, 'ä', 'ae'));
}
function decodeParam($parameter) {
  return Str::replace(urldecode($parameter), 'ae', 'ä');
}
?>

<main class="grid blocks">

<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

<div id="topics" class="program__sections">
  <h2><?= $page->topicsHeadline() ?></h2>
  <ul class="program__topic-list">
    <li>
      <a
        class="<?= classnames("program__topic-link", [
          "program__topic-link--active" => decodeParam(get("thema")) == ""
        ]) ?>"
        href="<?=$page->url() ?>#topics"
      >Alle</a>
    </li>
  <?php foreach ($page->topics()->split() as $category): ?>
    <li>
      <a
        class="<?= classnames("program__topic-link", [
          "program__topic-link--active" => decodeParam(get("thema")) == decodeParam($category)
          ]) ?>"
        href="<?=$page->url() ?>?thema=<?= encodeParam($category) ?>#topics"
      ><?= $category ?></a>
    </li>
  <?php endforeach ?>
  </ul>
  <?php if ($page->pdfFile()->isNotEmpty() and $page->downloadLinkText()->isNotEmpty()): ?>
  <a href="<?= $page->pdfFile()->toFile()->url() ?>" class="program__download-link link">
    <?= $page->downloadLinkText() ?>
  </a>
  <?php endif; ?>
</div>

<?php if ($topic = decodeParam(get("thema"))): ?>
<div class="program__topic-wrapper grid blocks">

<?php
  foreach ($page->children() as $chapter):
  $blocks = $chapter->blocks()->toBlocks()->filterBy("tags", $topic, ",");
?>

  <?php if ($blocks->isNotEmpty()): ?>
  <p class="program__chapter-label"><?= $page->chapterName() ?> <?= $chapter->indexOf() + 1 ?>: <?= $chapter->title() ?></p>

  <?php foreach ($blocks as $block): ?>
    <?php snippet('blocks/' . $block->type(), [
      'block' => $block,
      'page' => $chapter
    ]) ?>
  <?php endforeach; ?>

  <p><a href="<?= $chapter->url() ?>" class="link"><?= $page->readFullChapter() ?></a></p>
  <?php endif; ?>

<?php endforeach; ?>
</div>

<?php else: ?>

<div class="program__chapters-wrapper grid">
  <div class="program__chapters">
  <?php foreach ($page->children() as $chapter): ?>
    <a class="program__chapter" href="<?= $chapter->url() ?>">
      <h3><?= $chapter->title() ?></h3>
      <p><?= $chapter->description() ?></p>
      <div class="program__read-more"><?= $page->readMore() ?></div>
    </a>
  <?php endforeach ?>
  </div>
</div>

<?php endif; ?>

<div class="program__follow-up">
  <div>
    <h2><?= $page->leftHeadline() ?></h2>
    <div><?= $page->leftText() ?></div>
    <a class="button" href="<?= $page->leftLink() ?>">
      <?= $page->leftLinkText() ?>
    </a>
  </div>
  <div>
    <h2><?= $page->rightHeadline() ?></h2>
    <div><?= $page->rightText() ?></div>
    <a class="button" href="<?= $page->rightLink() ?>">
      <?= $page->rightLinkText() ?>
    </a>
  </div>
</div>

</main>

<script type="module">
const links = Array.from(document.querySelectorAll(".program-chapter__anchor"));
links.forEach(link => {
  link.addEventListener('click', event => {
    event.preventDefault();
    copyTextToClipboard(link.href);
    link.classList.add('program-chapter__anchor--copied');
    setTimeout(() => {
      link.classList.remove('program-chapter__anchor--copied');
    }, 1500);
  });
});

function copyTextToClipboard(input, {target = document.body} = {}) {
	const element = document.createElement('textarea');
	const previouslyFocusedElement = document.activeElement;

	element.value = input;

	// Prevent keyboard from showing on mobile
	element.setAttribute('readonly', '');

	element.style.contain = 'strict';
	element.style.position = 'absolute';
	element.style.left = '-9999px';
	element.style.fontSize = '12pt'; // Prevent zooming on iOS

	const selection = document.getSelection();
	let originalRange = false;
	if (selection.rangeCount > 0) {
		originalRange = selection.getRangeAt(0);
	}

	target.append(element);
	element.select();

	// Explicit selection workaround for iOS
	element.selectionStart = 0;
	element.selectionEnd = input.length;

	let isSuccess = false;
	try {
		isSuccess = document.execCommand('copy');
	} catch {}

	element.remove();

	if (originalRange) {
		selection.removeAllRanges();
		selection.addRange(originalRange);
	}

	// Get the focus back on the previously focused element, if any
	if (previouslyFocusedElement) {
		previouslyFocusedElement.focus();
	}

	return isSuccess;
}
</script>

<?php snippet("footer") ?>

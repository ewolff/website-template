<?php snippet("header") ?>

<main class="grid blocks">

<div class="livestream grid">
  <h1 class="livestream__title"><?= $page->title() ?></h1>
  <time class="livestream__date"><?= \Carbon\Carbon::parse($page->date()->toDate())->locale(I18n::locale())->isoFormat("DD. MMMM YYYY") ?></time>
  <div class="livestream__container">
    <a id="youtube-<?=$page->youtubeID() ?>" href="https://www.youtube.com/watch?v=<?=$page->youtubeID() ?>" class="livestream__video">
      <img class="livestream__thumbnail" src="<?= $page->thumbnail()->toFile()->url() ?>" alt="">
      <img class="livestream__play-button" src="/assets/images/play.svg" alt="">
    </a>
  </div>
  <script type="module">
    const mediaElement = document.querySelector('#youtube-<?=$page->youtubeID() ?>');
    mediaElement.addEventListener('click', (event) => {
      event.preventDefault();
      mediaElement.innerHTML = '<iframe frameborder="0" src="https://youtube.com/embed/<?=$page->youtubeID() ?>?autoplay=1" allow="autoplay" allowfullscreen="allowfullscreen"></iframe>';
      mediaElement.classList.add('livestream__video--playing')
    }, false);
  </script>
</div>

<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

<?php snippet("sibling-navigator", ["page" => $page]) ?>

</main>

<?php snippet("footer") ?>

<?php snippet("header", ["disable_auto_tel_detection" => true]) ?>

<main class="donations grid blocks">

<div class="donations__intro grid">
  <div class="donations__intro-text">
    <h1><?= $page->headline() ?></h1>
    <?= $page->description() ?>
  </div>

  <div class="donations__intro-widget">
    <?= $page->widgetCode() ?>
  </div>
</div>

<div class="donations__block grid">
  <h2 class="donations__headline"><?= $page->additionalHeadline() ?></h2>

  <div class="donations__box donations__box--small">
    <h3><?= $page->paypalHeadline() ?></h3>
    <img class="donations__box-image" src="<?= $page->paypalQRCode()->toFile()->url() ?>">
    <div class="donations__box-button">
      <a class="button" href="<?= $page->paypalButtonLink() ?>"><?= $page->paypalButtonText() ?></a>
    </div>
  </div>

  <div class="donations__box donations__box--large">
    <h3 class="donations__box-headline"><?= $page->giroHeadline() ?></h3>

    <div class="donations__box-giro">
      <div><?= $page->giroReceiver() ?></div>
      <div><?= $page->giroIBAN() ?></div>
      <div><?= $page->giroBIC() ?></div>
      <div><?= $page->giroBank() ?></div>

      <h4>Verwendungszweck</h4>
      <p><?= $page->giroReference() ?></p>

      <img class="donations__box-image" src="<?= $page->giroQRCode()->toFile()->url() ?>">
    </div>

  </div>
</div>

<div class="donations__outcome">
  <h2 class="donations__headline"><?= $page->outcomeHeadline() ?></h2>
  <div class="donations__outcome-list">
    <?= $page->outcomes() ?>
  </div>
</div>

<div class="donations__other-options grid">
  <div class="donations__passive-membership">
    <h2><?= $page->backersHeadline() ?></h2>
    <p class="donations__description"><?= $page->backersDescription() ?></p>
    <a class="button button--left" href="<?= $page->backersLink() ?>">
      <?= $page->backersLinkText() ?>
    </a>
  </div>
  <div class="donations__membership">
    <h2><?= $page->joinUsHeadline() ?></h2>
    <p class="donations__description"><?= $page->joinUsDescription() ?></p>
    <a class="button button--left" href="<?= $page->joinUsLink() ?>">
      <?= $page->joinUsLinkText() ?>
    </a>
  </div>
</div>


<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

</main>

<?php snippet("footer") ?>

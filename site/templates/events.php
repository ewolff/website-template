<?php snippet("header") ?>

<main class="events blocks grid">

<?php
  $futureEvents = $page->children()->filter(function ($event) {
    return $event->date()->toDate() > time() - 21600; # in the future or 6 hours ago
  })->sortBy('date', 'asc');
?>
  <h1 style="max-width: 800px"><?= $page->title() ?></h1>
  <div class="events__list">
  <?php foreach ($futureEvents as $event): ?>
  <?php snippet("event", [
    "event" => $event,
    "isBold" => true,
    ]);
  ?>
  <?php endforeach; ?>
  </div>

<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach; ?>

</main>

<?php snippet("footer") ?>

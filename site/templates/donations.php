<?php snippet("header", ["disable_auto_tel_detection" => true]) ?>

<main class="donations grid blocks">

<div class="donations__intro grid">
  <h1 class="donations__title"><?= $page->title() ?></h1>
  <p class="donations__intro-text"><?= $page->description() ?></p>
</div>

<div class="donations__block grid diagonal">
  <h2 class="donations__block-headline"><?= $page->headline() ?></h2>

  <div class="donations__box">
    <h3 class="donations__box-headline"><?= $page->giroHeadline() ?></h3>
    <p class="donations__box-description"><?= $page->giroDescription() ?></p>

    <dl class="donations__box-giro">
      <dt>Empfänger</dt>
      <dd><?= $page->giroReceiver() ?></dd>

      <dt>IBAN</dt>
      <dd><?= $page->giroIBAN() ?></dd>

      <dt>BIC</dt>
      <dd><?= $page->giroBIC() ?></dd>

      <dt>Bank</dt>
      <dd><?= $page->giroBank() ?></dd>

      <dt>Verwendungszweck</dt>
      <dd><?= $page->giroReference() ?></dd>
    </dl>

    <img class="donations__box-image" src="<?= $page->giroQRCode()->toFile()->url() ?>">
  </div>

  <div class="donations__box">
    <h3><?= $page->paypalHeadline() ?></h3>
    <p><?= $page->paypalDescription() ?></p>
    <a class="button" href="<?= $page->paypalButtonLink() ?>"><?= $page->paypalButtonText() ?></a>

    <img class="donations__box-image" src="<?= $page->paypalQRCode()->toFile()->url() ?>">
  </div>

  <div class="donations__box donations__box--half">
    <h3><?= $page->crowdfundingHeadline() ?></h3>
    <p><?= $page->crowdfundingDescription() ?></p>
    <a class="link" href="<?= $page->crowdfundingLink() ?>"><?= $page->crowdfundingLinkText() ?></a>
  </div>

  <div class="donations__box donations__box--half">
    <h3><?= $page->backersHeadline() ?></h3>
    <p><?= $page->backersDescription() ?></p>
    <a class="link" href="<?= $page->backersLink() ?>"><?= $page->backersLinkText() ?></a>
  </div>
</div>


<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

</main>

<?php snippet("footer") ?>

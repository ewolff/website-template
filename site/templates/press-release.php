<?php snippet("header") ?>

<main class="blocks grid">

<header class="text-intro">
  <h1><?= $page->title() ?></h1>
  <div class="text-intro__meta">
    <time class="text-intro__first" datetime="<?= $page->date() ?>"><?= \Carbon\Carbon::parse($page->date()->toDate())->locale(I18n::locale())->isoFormat("DD. MMMM YYYY") ?></time>
  </div>
</header>

<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

<div class="two-column-list">
  <h3>Pressekontakt</h3>
  <dl>
  <?php foreach ($page->parent()->pressContact()->toStructure() as $row): ?>
    <dt><?= $row->left() ?></dt>
    <dd><?= $row->right() ?></dd>
  <?php endforeach; ?>
  </dl>
</div>

<?php snippet("sibling-navigator", ["page" => $page]) ?>

</main>

<?php snippet("footer") ?>

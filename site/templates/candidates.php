<?php snippet("header") ?>

<main class="grid blocks">

<?php
  // Titel der Seite anzeigen, wenn keine Blöcke vorhanden sind
  // oder wenn eines der Intro-Module der erste Block ist
  $first = $page->blocks()->toBlocks()->first();
  if (!isset($first) || !in_array($first->type(), ["intro", "intro-with-columns"])):
?>
  <h1><?= $page->title() ?></h1>
<?php endif; ?>

<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

<?php snippet("district-list", ["list" => page('landesliste'), "limit" => 10, "style" => "bold"]) ?>

<?php foreach ($site->children()
  ->filterBy('intendedTemplate', 'district')
  ->filterBy('id', '!=', 'landesliste')
  ->sortBy('title', 'asc') as $list
): ?>
<?php snippet("district-list", ["list" => $list, "limit" => 10]) ?>
<?php endforeach ?>

</main>

<?php snippet("footer") ?>

<?php snippet("header") ?>

<main class="grid candidate">

<?php if ($photo = $page->previewImage()->toFile()): ?>
<img class="candidate__photo" src="<?= $photo->url() ?>">
<?php endif; ?>
<div class="candidate__info">
  <h1 class="candidate__name"><?= $page->title() ?></h1>
  <?php if($page->socialMediaAccounts()->isNotEmpty()): ?>
  <ul class="candidate__social-media">
    <?php foreach ($page->socialMediaAccounts()->toStructure() as $network): ?>
    <?php if ($image = $site->file($network->name().".svg")): ?>
    <li>
      <a href="<?=$network->link() ?>">
        <img
          class="candidate__social-network"
          src="<?=$image->url() ?>"
          alt="<?=$network->name() ?>">
      </a>
    </li>
    <?php endif; ?>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
  <dl class="candidate__details">
    <dt>Kandidatur:</dt>
    <?php
      $landesliste = page('landesliste')->candidates()->toPages();
      if ($landesliste->findOpen()
    ): ?>
    <dd>Landesliste Platz <?= $page->indexOf($landesliste) + 1 ?></dd>
    <?php endif; ?>
    <?php if($bvvliste =
      $site->children()
      ->filterBy('intendedTemplate', 'district')
      ->filterBy('id', '!=', 'landesliste')
      ->filter(function ($district) {
        return $district->candidates()->toPages()->findOpen();
      })
      ->first()
    ): ?>
    <dd>BVV <?= $bvvliste->title() ?> Platz <?= $page->indexOf($bvvliste->candidates()->toPages()) + 1 ?></dd>
    <?php endif; ?>
    <?php if ($page->job()->isNotEmpty()): ?>
    <dt>Beruf:</dt>
    <dd><?= $page->job() ?></dd>
    <?php endif; ?>
    <?php if ($page->email()->isNotEmpty()): ?>
    <dt>Kontakt:</dt>
    <dd><a href="mailto:<?= $page->email() ?>"><?= $page->email() ?></a></dd>
    <?php endif; ?>
    <?php if ($page->topics()->isNotEmpty()): ?>
    <dt>Schwerpunktthemen:</dt>
    <dd><?= $page->topics() ?></dd>
    <?php endif; ?>
  </dl>
</div>


  <div class="blocks">
    <?php foreach ($page->blocks()->toBlocks() as $block): ?>
      <?= $block ?>
    <?php endforeach ?>
  </div>

</main>

<?php snippet("footer") ?>

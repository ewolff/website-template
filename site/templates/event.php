<?php snippet("header") ?>

<main class="blocks grid">

<?php snippet("event-intro", ["event" => $page]); ?>

<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

</main>

<?php snippet("footer") ?>

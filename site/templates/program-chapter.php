<?php snippet("header") ?>

<main class="program-chapter grid blocks">

<header class="text-intro">
  <h1><?= $page->title() ?></h1>
  <div class="text-intro__meta">
    <span class="text-intro__first"><?= $page->parent()->chapterSubline() ?></span>
    <span class="text-intro__second"><?= $page->parent()->chapterName() ?> <?= $page->indexOf() + 1 ?></span>
  </div>
</header>

<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

<?php snippet("sibling-navigator", ["page" => $page]) ?>

</main>

<script type="module">
const links = Array.from(document.querySelectorAll(".program-chapter__anchor"));
links.forEach(link => {
  link.addEventListener('click', event => {
    event.preventDefault();
    copyTextToClipboard(link.href);
    link.classList.add('program-chapter__anchor--copied');
    setTimeout(() => {
      link.classList.remove('program-chapter__anchor--copied');
    }, 1500);
  });
});

function copyTextToClipboard(input, {target = document.body} = {}) {
	const element = document.createElement('textarea');
	const previouslyFocusedElement = document.activeElement;

	element.value = input;

	// Prevent keyboard from showing on mobile
	element.setAttribute('readonly', '');

	element.style.contain = 'strict';
	element.style.position = 'absolute';
	element.style.left = '-9999px';
	element.style.fontSize = '12pt'; // Prevent zooming on iOS

	const selection = document.getSelection();
	let originalRange = false;
	if (selection.rangeCount > 0) {
		originalRange = selection.getRangeAt(0);
	}

	target.append(element);
	element.select();

	// Explicit selection workaround for iOS
	element.selectionStart = 0;
	element.selectionEnd = input.length;

	let isSuccess = false;
	try {
		isSuccess = document.execCommand('copy');
	} catch {}

	element.remove();

	if (originalRange) {
		selection.removeAllRanges();
		selection.addRange(originalRange);
	}

	// Get the focus back on the previously focused element, if any
	if (previouslyFocusedElement) {
		previouslyFocusedElement.focus();
	}

	return isSuccess;
}
</script>

<?php snippet("footer") ?>

<?php snippet("header") ?>

<main class="grid blocks">

<h1><?= $page->title() ?></h1>

<?php if (isset($success) && !$success): ?>
<p class="newsletter__error">
  <?= $page->errorMessage() ?>
</p>
<?php endif; ?>

<?php if (isset($success) && $success): ?>
<p class"newsletter__success">
  <?= $page->successMessage() ?>
</p>
<?php endif; ?>

<?php if (option('debug') and isset($response)): ?>
<?= dump($response) ?>
<?php endif; ?>

<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

</main>

<?php snippet("footer") ?>

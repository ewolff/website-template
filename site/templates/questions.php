<?php snippet("header") ?>

<main class="grid blocks">

<h1><?= $page->title() ?></h1>

<ul class="questions">
<?php foreach ($page->children() as $question): ?>
  <li class="questions__question">
    <a href="<?= $question->url() ?>">
    <?= $question->title() ?>
    </a>
  </li>
<?php endforeach ?>
</ul>

<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

</main>

<?php snippet("footer") ?>

<?php snippet("header") ?>

<main class="collection">

<div class="grid">
  <h1 class="collection__headline"><?= page("aktuelles")->title() ?></h1>
  <ul class="collection__list">
    <li>
      <a class="collection__link" href="<?= page("aktuelles")->url() ?>">Alle</a>
    </li>
  <?php foreach (page("aktuelles")->categories()->toPages() as $category): ?>
    <li>
      <a class="<?=classNames("collection__link", ["collection__link--active" => $category->isActive()]) ?>" href="<?= $category->url() ?>"><?= $category->title() ?></a>
    </li>
  <?php endforeach ?>
  </ul>
  <?php
    snippet("collection-items", [
      "items" => $page->children()->sortBy('date', 'desc')
    ])
  ?>
</div>

<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

</main>

<?php snippet("footer") ?>

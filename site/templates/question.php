<?php snippet("header") ?>

<main class="grid blocks">


<h1><?= $page->title() ?></h1>

<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

<div class="question__navigation">
<?php if ($prevPage = $page->prevListed()): ?>
  <div class="question__navigation-left">
    <strong>Vorherige:</strong>
    <p><a href="<?= $prevPage->url() ?>"><?= $prevPage->title() ?></a></p>
  </div>
  <?php endif; ?>
  <?php if ($nextPage = $page->nextListed()): ?>
  <div class="question__navigation-right">
    <strong>Nächste:</strong>
    <p><a href="<?= $nextPage->url() ?>"><?= $nextPage->title() ?></a></p>
  </div>
  <?php endif; ?>

  <a class="question__navigation-all link" href="<?= $page->parent()->url() ?>">
    alle häufigen Fragen anzeigen
  </a>
</div>

</main>

<?php snippet("footer") ?>

<?php snippet("header") ?>

<main class="collection grid blocks">

  <div class="grid">
    <h1 class="collection__headline"><?= $page->title() ?></h1>
    <ul class="collection__list">
      <li>
        <a class="collection__link collection__link--active" href="./">Alle</a>
      </li>
    <?php foreach ($page->categories()->toPages() as $category): ?>
      <li>
        <a class="collection__link" href="<?= $category->url() ?>"><?= $category->title() ?></a>
      </li>
    <?php endforeach ?>
    </ul>
    <?php
      snippet("collection-items", [
        "items" => $page->categories()->toPages()->children()->sortBy('date', 'desc')
      ])
    ?>
  </div>

<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

</main>

<?php snippet("footer") ?>

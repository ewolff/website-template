<?php snippet("header") ?>

<main class="grid blocks">

<header class="text-intro">
  <h1><?= $page->title() ?></h1>
  <div class="text-intro__meta">
    <time class="text-intro__first" datetime="<?= $page->date() ?>"><?= \Carbon\Carbon::parse($page->date()->toDate())->locale(I18n::locale())->isoFormat("DD. MMMM YYYY") ?></time>
    <span class="text-intro__second">von <?= $page->author() ?></span>
  </div>
  <?php if ($image = $page->previewImage()->toFile()): ?>
  <img class="text-intro-image" src="<?= $image->url() ?>" alt="">
  <?php endif; ?>
</header>

<?php foreach ($page->blocks()->toBlocks() as $block): ?>
  <?= $block ?>
<?php endforeach ?>

<?php snippet("sibling-navigator", ["page" => $page]) ?>

</main>

<?php snippet("footer") ?>

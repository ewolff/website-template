# Website Template Klimaliste

Our website is build using Kirby CMS.

## Requirements

- PHP 7.3+
- PHP curl extension (e.g. `sudo apt-get install php7.4-curl`)
- PHP gd extension (e.g. `sudo apt-get install php7.4-gd`)
- Composer
- Webserver that parses .htaccess (e.g. Apache)

## Development

Clone the git repo and run `composer install`. Then run `composer start` to start a local development server.

## Deployment

- Unpack the git repo into a directory served by the web server
- (optional) Modify RewriteBase in `.htaccess` if the installation is
  not in the root directory.
- (optional) Kirby comes with a panel. Enable it as describe at
  https://getkirby.com/docs/reference/system/options/panel#allow-the-panel-to-be-installed-on-a-remote-server
  . You can access the panel at the URL `/panel/`. You can set up a
  new user even though this looks like a login page.

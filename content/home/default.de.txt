Title: Startseite

----

Blocks: [{"content":{"image":["1-guendungsparteitag.jpg"],"headline":"radikal:klima ist die Partei f\u00fcr ein <u>klimapositives<\/u> und <u>sozial-gerechtes<\/u> Berlin","text":"Als Teil der Klimaliste Deutschland treten wir zur Wahl des Berliner Abgeordnetenhauses im September 2021 an.","showsocialnetworks":"true"},"id":"4ee3a0f3-9da5-4b69-a120-3ada2d2afe68","isHidden":false,"type":"intro"},{"content":{"style":"neutral","headline":"Unsere Vision","text":"<p>Wir sind eine seit Januar 2020 stetig wachsende Gruppe von aktuell \u00fcber 150 entschlossenen Berliner:innen, die sich als Teil der Klimagerechtigkeitsbewegung f\u00fcr eine klimapositive Stadt einsetzen. Uns vereint die \u00dcberzeugung, dass die Klimakatastrophe vermeidbar ist, jedoch nur durch einen radikalen gesellschaftlichen Wandel in den n\u00e4chsten 10 Jahren. Dazu ben\u00f6tigt es ein ambitioniertes Programm, das wir zusammen mit Kiezbewohner:innen, st\u00e4dtischen Initiativen, Wissenschaftler:innen und unabh\u00e4ngigen Expert:innen gemeinsam entwickeln und als Teil der Klimaliste ins Abgeordnetenhaus von Berlin tragen.<\/p>","chosenimages":["ich-bin-radikal-weil-01.png","ich-bin-radikal-weil-10-768x768.png","ich-bin-radikal-weil-09.png","ich-bin-radikal-weil-19-768x768.png","ich-bin-radikal-weil-15-768x768.png","ich-bin-radikal-weil-14-768x768.png","ich-bin-radikal-weil-11-768x768.png","ich-bin-radikal-weil-06-768x768.png","ich-bin-radikal-weil-16.png"],"imagecolumns":3,"links":[{"text":"mehr \u00fcber uns","external":"false","page":["ueber-uns"],"link":""},{"text":"mitmachen","external":"false","page":["mitmachen"],"link":""}]},"id":"ff8e91d0-c319-4f68-b72e-8ea679748a00","isHidden":false,"type":"text-and-image"},{"content":{"style":"bold","headline":"H\u00e4ufige Fragen","maximumitems":10,"sitelinktext":"weiterlesen","automaticpages":"true","chosenparentpages":["fragen"],"chosenspecificpages":[],"morelink":["fragen"],"linktext":"Alle h\u00e4ufigen Fragen"},"id":"38c10692-c77e-4f8e-bf24-d1c7a4308fbd","isHidden":false,"type":"carousel"},{"content":{"style":"neutral","headline":"Aktuelles","maximumitems":10,"sitelinktext":"","automaticpages":"true","chosenparentpages":["blog","livestreams","presse","pressemitteilungen"],"chosenspecificpages":["pressemitteilungen\/offener-brief-an-die-gruenen-mitglieder-und-waehler-innen"],"morelink":["aktuelles"],"linktext":"Alles Aktuelle"},"id":"b38b18e4-50a1-4ad7-b0ca-98894ae25023","isHidden":false,"type":"carousel"},{"content":{"style":"bold","headline":"Termine","linktext":"Alle Termine anzeigen","maximumitems":3,"filtercategory":""},"id":"a03599f6-9ed3-4c34-817c-15bf3ff4efed","isHidden":false,"type":"events-preview"},{"content":{"style":"neutral"},"id":"45cfb86f-8c7e-44d7-9c6f-d6c6ef2632cc","isHidden":false,"type":"countdown"}]

----

Description: 

----

Previewimage: 

----

Navigationstyle: whiteAndActiveIsWhite

----

Shownavigationgradient: true

----

Beschreibung: 

----

Vorschaubild: - 1-guendungsparteitag.jpg

----

Hidenavigationgradient: true

----

Layout:

-
  attrs: [ ]
  columns:
    -
      blocks:
        -
          content:
            level: h2
            text: ""
          id: 1c96216c-459c-43fa-b170-70b59f654044
          isHidden: false
          type: heading
      id: 6d2e82d1-c406-4c71-81e3-aaf88aaa8bdd
      width: 1/3
    -
      blocks: [ ]
      id: 77d10872-74b0-4478-a4d0-6e74bbed7e18
      width: 2/3
  id: 8e8bb6d1-b87e-453c-91f2-cf692ee217f9
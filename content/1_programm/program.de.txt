Title: Programm

----

Blocks: [{"content":{"image":["placeholder-wide-2.jpg"],"headline":"Unser <u>Programm<\/u> Lorem ipsum","text":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.","showsocialnetworks":"false"},"id":"899fa4e2-c701-417c-8a47-129de81c72ff","isHidden":false,"type":"intro"}]

----

Topicsheadline: Welche Themen interessieren dich?

----

Topics: Klimaschutz, Energie, Bauen & Wohnen, Mobilität, Wirtschaft, Ernährung, Gesundheit, Bildung, Digitalisierung, Finanzen, Kultur, Inklusion, Partizipation

----

Readmore: weiterlesen

----

Leftheadline: Block links

----

Rightheadline: Block rechts

----

Lefttext: <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>

----

Righttext: <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>

----

Leftlink: https://klimaliste.de

----

Rightlink: https://klimaliste.de

----

Leftlinktext: Call-to-Action

----

Rightlinktext: Call-to-Action

----

Description: 

----

Previewimage: 

----

Pdffile: 

----

Downloadlinktext: 

----

Chaptersubline: Wahlprogramm 2021

----

Chaptername: Kapitel

----

Nextpage: nächstes Kapitel

----

Previouspage: vorheriges Kapitel

----

Readfullchapter: ganzes Kapitel lesen

----

Navigationstyle: 

----

Shownavigationgradient: false
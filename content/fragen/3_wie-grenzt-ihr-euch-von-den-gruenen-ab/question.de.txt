Title: Wie grenzt ihr euch von den Grünen ab?

----

Blocks: [{"content":{"text":"<p>Wir machen Berlin auf sozial-gerechte Weise klimapositiv indem wir unser Wirtschaftssystem transformieren. Weg von Verschwendung, hin zu einer Kreislaufwirtschaft und aus der Klimabewegung heraus.<\/p>"},"id":"192c701c-d6d6-4d94-9de1-48d6e17060ee","isHidden":false,"type":"text"}]

----

Description: Wir machen Berlin auf sozial-gerechte Weise klimapositiv indem wir unser Wirtschaftssystem transformieren. Weg von Verschwendung, hin zu einer Kreislaufwirtschaft und aus der Klimabewegung heraus.

----

Previewimage: 

----

Navigationstyle: 

----

Shownavigationgradient: false

----

Vorschaubild: 